# argus-server [![Build Status](https://travis-ci.com/hephyvienna/ansible-role-argus-server.svg?branch=master)](https://travis-ci.com/hephyvienna/ansible-role-argus-server) [![Ansible Role](https://img.shields.io/ansible/role/40999.svg)](https://galaxy.ansible.com/hephyvienna/argus_server)

Install and configure EGI Argus server

## Requirements

*   EL6/7
*   EPEL

## Role Variables

    argus_server_host:

Hostname of the server

    argus_bind_host:

Can be different from argus_server_host on OpenStack with floating addresses

    argus_server_host_dn: /DC=org/DC=terena/DC=tcs/C=AT/ST=Wien/L=Wien/O=Oesterreichische Akademie der Wissenschaften/CN=argus.hephy.oeaw.ac.at

    argus_server_paps: {}

Define the PAPs. For details check the [documentation](https://argus-documentation.readthedocs.io/en/stable/pap/configuration.html).

    argus_server_paps:
      - name: centralbanning
        type: remote
        enabled: true
        dn: ...
        hostname: ...
        port: 8150
        path: /pap/services/
        protocol: https
        public: true

Example for defining central banning.

    argus_server_pap_policies:
      - resource: http://authz-interop.org/xacml/resource/resource-type/wn
        action: http://glite.org/xacml/action/execute
        permit:
          - pfqan = "/cms/Role=lcgadmin"
          - pfqan = "/cms/Role=production"
          - pfqan = "/cms/Role=pilot"
          - vo = "cms"
      - resource: http://authz-interop.org/xacml/resource/resource-type/ce
        action: .*
        permit:
          - vo = "cms"

## Dependencies

*   _hephyvienna.grid_ - setup of repository and grid security
*   _hephyvienna.poolaccounts_ - setup of poolaccounts

## Example Playbook

    - hosts: servers
      roles:
         - hephyvienna.argus-server

## License

MIT

## Author Information

Written by [Dietrich Liko](http://hephy.at/dliko) in June 2019

[Institute for High Energy Physics](http://www.hephy.at) of the
[Austrian Academy of Sciences](http://www.oeaw.ac.at)
