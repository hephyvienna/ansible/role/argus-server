import os

import testinfra.utils.ansible_runner
import pytest

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


@pytest.mark.parametrize('name,version', [
    ('java-1.8.0-openjdk', '1.8.0.'),
    ('argus-authz', '1.7.'),
    ('bdii', '5.2.'),
    ('glite-info-provider-service', '1.13.'),
])
def test_package(host, name, version):
    pkg = host.package(name)
    assert pkg.is_installed
    assert pkg.version.startswith(version)


@pytest.mark.parametrize('name', [
    '/etc/argus/pap/pap-admin.properties',
    '/etc/argus/pap/pap_configuration.ini',
    '/etc/argus/pdp/pdp.ini',
    '/etc/argus/pepd/pepd.ini',
])
def test_config_file(host, name):
    f = host.file(name)

    assert f.exists


@pytest.mark.parametrize('name', [
    'argus-pap',
    'argus-pdp',
    'argus-pepd',
])
def test_service(host, name):

    svc = host.service(name)

    assert svc.is_running
    assert svc.is_enabled
